name := "weather-app"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies += "io.reactivex" %% "rxscala" % "0.26.5"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.8.2"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.8.2"
libraryDependencies += "com.jfoenix" % "jfoenix" % "1.3.0"
libraryDependencies += "io.reactivex" % "rxjavafx" % "1.1.0"
libraryDependencies += "org.kordamp.ikonli" % "ikonli-javafx" % "1.9.0"
libraryDependencies += "org.kordamp.ikonli" % "ikonli-weathericons-pack" % "1.9.0"
libraryDependencies += "org.kordamp.ikonli" % "ikonli-fontawesome-pack" % "1.9.0"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.0"
libraryDependencies += "org.ocpsoft.prettytime" % "prettytime" % "4.0.0.Final"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"