import event.{NetworkRequestFinishedEvent, NetworkRequestIssuedEvent}
import network.WeatherDataSource
import org.scalatest.{FlatSpec, Matchers}
import rx.lang.scala.Observable

/**
  * Created by kamil on 13/06/2017.
  */
class DataSourceSpec extends FlatSpec with Matchers {

  "DataSource" should "convert signal into request" in {
    val dataSource = new WeatherDataSource
    val input = Observable.just(1)

    val result = dataSource.wrapRequest(input).toList.toBlocking.first
    result.head should be(NetworkRequestIssuedEvent())
    result(2) should be(NetworkRequestFinishedEvent())

  }

}
