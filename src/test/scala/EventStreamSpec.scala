import java.util.concurrent.TimeUnit

import event.{EventStream, SettingsRequestEvent, WeatherEvent, WeatherRateEvent}
import org.scalatest.{FlatSpec, Matchers}
import rx.lang.scala.Observable
import rx.lang.scala.observers.TestSubscriber

import scala.concurrent.duration.FiniteDuration


class EventStreamSpec extends FlatSpec with Matchers {

  "EventStram" should "connect observable to itself" in {
    val data: Option[Float] = Some(1)
    val inputData = WeatherRateEvent(data, data, data, data, data, data)
    EventStream.joinStream(Observable.just(inputData))
    val result = EventStream.events
    result.subscribe(a => a should be(inputData))
  }

  "EventSteam" should "disconnect observable" in {
    val data = Observable.just(SettingsRequestEvent())
    val sub = EventStream.joinStream(data)
    sub.unsubscribe()
    val res = EventStream.events.isEmpty.toBlocking.single
    res should be(true)
  }

}