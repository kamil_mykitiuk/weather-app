package control

import java.text.DecimalFormat
import javafx.beans.property.{ObjectProperty, SimpleObjectProperty, SimpleStringProperty}
import javafx.scene.layout.{HBox, Pane}
import javafx.scene.text.Text

import event.RawRateEvent
import org.kordamp.ikonli.javafx.FontIcon
import org.kordamp.ikonli.weathericons.WeatherIcons
import rx.lang.scala.Observable

/**
  * Information value controller
  */
class InfoValueControl extends Pane() {

  private var noDataIcon: FontIcon = new FontIcon(WeatherIcons.NA)
  noDataIcon.getStyleClass.add("no-data")
  getChildren.add(noDataIcon)


  private var innerContainer: HBox = _

  private var prefixLabel: Text = _
  private var suffixLabel: Text = _
  private var textControl: Text = _

  private val prefixProperty = new SimpleStringProperty
  private val suffixProperty = new SimpleStringProperty
  private val titleProperty = new SimpleStringProperty("-")

  private var formatPattern = "0.0"
  private var format = new DecimalFormat(formatPattern)


  var sourceProperty: ObjectProperty[Observable[RawRateEvent]] = new SimpleObjectProperty()

  def getSource: Observable[RawRateEvent] = sourceProperty.get()

  /**
    * subscribe String labels to Observable of data, if there is no data - show noDataIcon
    */
  def setSource(source: Observable[RawRateEvent]): Unit = {
    source.subscribe(e => {
      if (innerContainer == null) {
        createContentControl()
      }
      if (e.value.isEmpty) {
        innerContainer.setVisible(false)
        noDataIcon.setVisible(true)
      } else {
        noDataIcon.setVisible(false)
        innerContainer.setVisible(true)
        textControl.setText(format.format(e.getValue.get))
      }
    })

    sourceProperty.set(source)
  }

  def getPrefix: String = prefixProperty.get

  def setPrefix(prefix: String): Unit = {
    prefixProperty.set(prefix)
  }

  def getSuffix: String = suffixProperty.get

  def setSuffix(sufix: String): Unit = {
    suffixProperty.set(sufix)
  }

  /**
    * create containers for exhibited data
    */
  private def createContentControl(): Unit = {
    textControl = new Text()
    textControl.getStyleClass.add("rate-value")

    prefixLabel = new Text
    prefixLabel.textProperty.bind(prefixProperty)
    prefixLabel.getStyleClass.add("helper-label")

    suffixLabel = new Text
    suffixLabel.textProperty.bind(suffixProperty)
    suffixLabel.getStyleClass.add("helper-label")

    innerContainer = new HBox
    innerContainer.getStyleClass.add("value-container")
    innerContainer.getChildren.addAll(prefixLabel, textControl, suffixLabel)

    getChildren.add(innerContainer)

  }

  /**
    * Make exhibited data centered
    */
  override protected def layoutChildren(): Unit = {
    /* Custom children positioning */ super.layoutChildren()
    if (noDataIcon.isVisible) noDataIcon.relocate((getWidth - noDataIcon.getLayoutBounds.getWidth) / 2, (getHeight - noDataIcon.getLayoutBounds.getHeight) / 2)
    if (innerContainer != null) innerContainer.relocate((getWidth - innerContainer.getLayoutBounds.getWidth) / 2, (getHeight - innerContainer.getLayoutBounds.getHeight) / 2)
  }

}
