package control

import javafx.beans.property.{ObjectProperty, ObjectPropertyBase}
import javafx.beans.{DefaultProperty, Observable}
import javafx.collections.{FXCollections, ObservableList}
import javafx.fxml.{FXML, FXMLLoader}
import javafx.scene.Node
import javafx.scene.layout.{GridPane, StackPane}

import org.apache.logging.log4j.{LogManager, Logger}


/**
  * Container for Image icon and information value
  */
@DefaultProperty("rates")
class InfoPane extends StackPane {
  val FXML_TEMPLATE: String = "/fxml/info-pane.fxml"
  private var rates: ObservableList[Node] = FXCollections.observableArrayList()
  private var image: ObjectProperty[Node] = _

  /**
    * load fxml schema for this container and connect at runtime rest of elements
    */
  private def constructor() = {
    val loader = new FXMLLoader(classOf[InfoPane].getResource(FXML_TEMPLATE))
    loader.setController(this)
    try {
      getChildren.add(loader.load())
    } catch {
      case a: Throwable => throw new RuntimeException(a)
    }

    rates.addListener((o: Observable) => {
      containerGrid.add(rates.get(0), 1, 0)
    })
  }

  constructor()


  @FXML
  var imageContainer: StackPane = _

  @FXML
  private var containerGrid: GridPane = _

  /**
    * imageProperty class, it's is created when image is set and then connected to imageContainer
    */
  class imageBase extends ObjectPropertyBase[Node] {
    override def getName: String = "image"

    override def getBean: AnyRef = InfoPane.this

    override def invalidated(): Unit = {
      var children: ObservableList[Node] = InfoPane.this.imageContainer.getChildren
      if (children.size() > 0) children.clear()
      children.add(get())
      imageContainer.requestLayout()
    }
  }

  def imageProperty(): ObjectProperty[Node] = {
    if (image == null) {
      image = new imageBase
    }
    image
  }

  def getImage: Node = imageProperty().get()

  def setImage(imagea: Node): Unit = {
    imageProperty().set(imagea)
  }

  def getRates: ObservableList[Node] = rates

}
