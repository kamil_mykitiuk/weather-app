package network

import event.{WeatherRateEvent, WeatherEvent}
import rx.lang.scala.Observable

import scala.util.matching.Regex

/**
  * Data Source for meteo.waw.pl
  */
class MeteoDataSource extends DataSource {

  private val URL: String = "http://www.meteo.waw.pl/"

  //patterns for matching ccorrespondingvalues from html code
  private val paternComma: Regex = ",".r
  private val patternWindPower: Regex = "prędkość: <strong id=\"PARAM_WV\">(\\d*,\\d*)".r
  private val patternPressure: Regex = "ciśnienie: <strong id=\"PARAM_PR\">(\\d*,\\d*)".r
  private val patternWindDirection: Regex = "kierunek: <strong id=\"PARAM_WD\">(\\d*,\\d*)".r
  private val patternHumidity: Regex = "wilgotność: <strong id=\"PARAM_RH\">(\\d*,\\d*)".r
  private val patternTemp: Regex = "temperatura: <strong id=\"PARAM_TA\">(\\d*,\\d*)".r

  private def get(url: String): String = scala.io.Source.fromURL(url).mkString

  /**
    * try to parse string into float
    *
    * @param s
    * @return
    */
  private def parseDouble(s: Option[String]) = try {
    Some(paternComma.replaceAllIn(s.getOrElse("Not Found"), ".") toFloat)
  } catch {
    case _: Exception => None
  }

  private def patternMatch(pattern: Regex, data: String): Option[String] = for (m <- pattern.findFirstMatchIn(data)) yield m.group(1)

  private def getValue(pattern: Regex, data: String): Option[Float] = parseDouble(patternMatch(pattern, data))

  override protected def makeRequest[T]: Observable[_ <: WeatherEvent] = {
    val website = get(URL)
    val windPower = getValue(patternWindPower, website)
    val pressure = getValue(patternPressure, website)
    val windDirection = getValue(patternWindDirection, website)
    val humidity = getValue(patternHumidity, website)
    val temp = getValue(patternTemp, website)

    Observable.just(WeatherRateEvent(temp, pressure, humidity, windPower, windDirection, None))
  }
}
