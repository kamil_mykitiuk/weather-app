package network

import event.{AirRateEvent, WeatherEvent}
import rx.lang.scala.Observable

/**
  * Data Source for Air pollution
  */
class AirDataSource extends DataSource {

  private val URL = "http://powietrze.gios.gov.pl/pjp/current/getAQIDetailsList?param=AQI"

  private def get(url: String): String = scala.io.Source.fromURL(url).mkString

  override protected def makeRequest[T]: Observable[_ <: WeatherEvent] = {
    val jsonResponse = JsonHelper.asJsonArray(get(URL))
    val jsonCity = jsonResponse.get(12).getAsJsonObject.get("values").getAsJsonObject
    val PM10 = jsonCity.get("PM10").getAsFloat
    val PM25 = jsonCity.get("PM2.5").getAsFloat
    Observable.just(AirRateEvent(PM10, PM25))
  }

}
