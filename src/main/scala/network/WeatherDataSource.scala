package network

import event.{WeatherEvent, WeatherRateEvent}
import network.JsonHelper.asJsonObject
import rx.lang.scala.Observable

/**
  * Data source from openwathermap.org
  */
class WeatherDataSource extends DataSource {

  private val APIKEY = "2972823621f6d5207b8484284d46a927"
  private val CITY_ID = "6695624"
  private val BASE_URL: String = "http://api.openweathermap.org/data/2.5/weather?id="
  private val URL = BASE_URL + CITY_ID + "&appid=" + APIKEY
  private val celsiusZero: Float = 273.15.toFloat

  private def get(url: String): String = scala.io.Source.fromURL(url).mkString

  override protected def makeRequest[T]: Observable[_ <: WeatherEvent] = {
    val jsonResponse = asJsonObject(get(URL))
    val temp = jsonResponse.get("main").getAsJsonObject.get("temp").getAsFloat - celsiusZero
    val pressure = jsonResponse.get("main").getAsJsonObject.get("pressure").getAsFloat
    val humidity = jsonResponse.get("main").getAsJsonObject.get("humidity").getAsFloat
    val windSpeed = jsonResponse.get("wind").getAsJsonObject.get("speed").getAsFloat
    val windDirection = jsonResponse.get("wind").getAsJsonObject.get("deg").getAsFloat
    val clouds = jsonResponse.get("clouds").getAsJsonObject.get("all").getAsFloat
    Observable.just(WeatherRateEvent(Some(temp), Some(pressure), Some(humidity), Some(windSpeed), Some(windDirection), Some(clouds)))
  }
}