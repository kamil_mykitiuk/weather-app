package network


import event._
import org.apache.logging.log4j.{LogManager, Logger}
import rx.lang.scala.Observable
import rx.lang.scala.schedulers.IOScheduler

import scala.concurrent.duration.{DurationInt, FiniteDuration}

/**
  * Data Source
  */
abstract class DataSource {
  var log: Logger = LogManager.getLogger(classOf[DataSource])

  val POLL_INTERVAL: FiniteDuration = 60 seconds
  val INITIAL_DELAY: FiniteDuration = 3 seconds
  val TIMEOUT: FiniteDuration = 20 seconds

  /**
    * connected observable of request from buttons and internal clock
    *
    * @return
    */
  def dataSourceStream: Observable[_ <: WeatherEvent] = {
    wrapRequest(fixedIntervalStream).merge(wrapRequest(
      EventStream.eventsInIO.filter({
        case _: RefreshRequstEvent => true
        case _ => false
      })))
  }


  /**
    * Initial observable to be converted to request events
    *
    * @return
    */
  protected def fixedIntervalStream: Observable[Long] = Observable.interval(INITIAL_DELAY, POLL_INTERVAL, IOScheduler())

  /**
    * Main function which parse data from external source
    *
    * @tparam T
    * @return
    */
  protected def makeRequest[T]: Observable[_ <: WeatherEvent]

  /**
    * from each initial value in Observeable make combination of EventRequestIssued, Rate, EventRequestFinished
    * if Rate failed issue ErrorEvent
    *
    * @param obs
    * @tparam T
    * @return
    */
  def wrapRequest[T](obs: Observable[T]): Observable[_ <: WeatherEvent] = {
    obs.concatMap(_ => Observable.just(new NetworkRequestIssuedEvent) ++
      makeRequest.timeout(TIMEOUT).doOnError(s => log.error(s)).onErrorReturn(s => ErrorEvent(s)) ++
      Observable.just(new NetworkRequestFinishedEvent)
    )
  }
}