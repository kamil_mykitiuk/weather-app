package network

import com.google.gson.{JsonArray, JsonObject, JsonParser}

/**
  * Wrapped functions for parsing Json
  */
object JsonHelper {

  def asJsonObject(s: String): JsonObject = parse(s).getAsJsonObject

  def asJsonArray(s: String): JsonArray = parse(s).getAsJsonArray

  private def parse(s: String) = new JsonParser().parse(s)

}
