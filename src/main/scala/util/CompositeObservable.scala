package util

import rx.lang.scala.subjects.{PublishSubject, SerializedSubject}
import rx.lang.scala.{Observable, Subscription}
import rx.lang.scala.subscriptions.CompositeSubscription

/**
  * Utility class for composite observable, which can be at any moment unsubscribed. Not part of RxScala
  *
  * @tparam T L type of Observed vaules
  */
final class CompositeObservable[T]() {
  var subject: SerializedSubject[T] = PublishSubject[T]().toSerialized

  def toObservable: Observable[T] = subject

  def add(observable: Observable[T]): Subscription = observable.subscribe(this.subject)

  def addAll(observables: Observable[T]*): CompositeSubscription = {
    val subscriptions = CompositeSubscription()
    observables.map(ob => add(ob)).foreach(subscriptions += _)
    subscriptions
  }

}
