import java.io.IOException
import javafx.application.{Application, Platform}
import javafx.fxml.{FXML, FXMLLoader}
import javafx.scene.control.Button
import javafx.scene.layout.StackPane
import javafx.scene.{Parent, Scene}
import javafx.stage.Stage

import com.jfoenix.controls.{JFXDecorator, JFXDialog}
import event.{EventStream, SettingsRequestEvent, WeatherEvent}
import network.{AirDataSource, DataSource, MeteoDataSource, WeatherDataSource}
import org.apache.logging.log4j.{LogManager, Logger}
import rx.lang.scala.{JavaConversions, Observable, Subscription}
import rx.observables.JavaFxObservable


class Gui extends Application {

  private val FXML_MAIN_FROM_TEMPLATE = "/fxml/weather-main.fxml"
  private val FXML_CLOSE_DIALOG_TEMPLATE = "/fxml/close-dialog.fxml"
  private val FXML_SETTINGS_DIALOG_TEMPLATE = "/fxml/setting-dialog.fxml"

  private val FONT_CSS = "/css/jfoenix-fonts.css"
  private val MATERIAL_CSS = "/css/jfoenix-design.css"
  private val JFX_CSS = "/css/jfx.css"

  val logger: Logger = LogManager.getLogger(classOf[Gui])

  /**
    * class of setting window
    */
  private class SettingsDialogControler {
    @FXML
    private var dialog: JFXDialog = _

    @FXML
    private var openWeather: Button = _

    @FXML
    private var meteo: Button = _

    @FXML
    private def initialize = {
      JavaConversions toScalaObservable JavaFxObservable.actionEventsOf(openWeather) subscribe (_ => {
        logger.info("changing source to openWeather")
        mainSource.unsubscribe()
        mainSource = EventStream.joinStream((new WeatherDataSource).dataSourceStream)
        dialog.close()
      })

      JavaConversions toScalaObservable JavaFxObservable.actionEventsOf(meteo) subscribe (_ => {
        logger.info("changing source to meteo")
        mainSource.unsubscribe()
        mainSource = EventStream.joinStream((new MeteoDataSource).dataSourceStream)
        dialog.close()
      })
    }

    def show(pane: StackPane): Unit = {
      dialog.show(pane)
    }
  }

  private var settingsDialogController: SettingsDialogControler = _

  /**
    * controller of setting window
    */
  private def onSettings(): Unit = {
    if (settingsDialogController == null) {
      settingsDialogController = new SettingsDialogControler()
      val loader = new FXMLLoader(classOf[Gui].getResource(FXML_SETTINGS_DIALOG_TEMPLATE))
      loader.setController(settingsDialogController)
      try {
        loader.load()
      } catch {
        case e: IOException => {
          logger.error(e)
          throw new RuntimeException(e)
        }
      }
    }
    settingsDialogController.show(getMainPane)
  }

  private class CloseDialogControler {
    @FXML
    private var dialog: JFXDialog = _

    @FXML
    private var acceptButton: Button = _

    @FXML
    private var cancelButton: Button = _

    @FXML
    private def initialize = {
      JavaConversions toScalaObservable JavaFxObservable.actionEventsOf(acceptButton) subscribe (_ => {
        logger.info("Exiting...")
        Gui.this.mainStage.close()
      })

      JavaConversions toScalaObservable JavaFxObservable.actionEventsOf(cancelButton) subscribe (_ => {
        dialog.close()
      })
    }

    def show(pane: StackPane): Unit = {
      dialog.show(pane)
    }

  }

  private var closeDialogController: CloseDialogControler = _
  var mainStage: Stage = _
  var mainSource: Subscription = _


  override def start(primaryStage: Stage): Unit = {
    logger.info("Start Weather application...")


    setupDataSources()

    setupEventHandler()

    setupTextRendering()

    logger.info("loading xml...")
    mainStage = primaryStage
    val pane: Parent = FXMLLoader.load(classOf[Gui].getResource(FXML_MAIN_FROM_TEMPLATE))
    val decorator: JFXDecorator = new JFXDecorator(mainStage, pane, false, false, true)


    decorator.setOnCloseButtonAction(() => onClose())

    val scene: Scene = new Scene(decorator)
    scene.setFill(null)
    scene.getStylesheets.addAll(classOf[Gui].getResource(FONT_CSS).toExternalForm,
      classOf[Gui].getResource(MATERIAL_CSS).toExternalForm,
      classOf[Gui].getResource(JFX_CSS).toExternalForm)

    mainStage.setScene(scene)
    mainStage.setWidth(600)
    mainStage.setHeight(400)
    mainStage.setResizable(false)
    mainStage.show()
    logger.info("Application's up and running")
  }

  /**
    * Close window controller
    */
  private def onClose(): Unit = {
    logger.info("onClose")

    if (closeDialogController == null) {
      closeDialogController = new CloseDialogControler()
      val loader = new FXMLLoader(classOf[Gui].getResource(FXML_CLOSE_DIALOG_TEMPLATE))
      loader.setController(closeDialogController)
      try {
        loader.load()
      } catch {
        case e: IOException => {
          logger.error(e)
          throw new RuntimeException(e)
        }
      }
    }

    closeDialogController.show(getMainPane)
  }

  private def getMainPane: StackPane = {
    logger.info("getMainPane")
    mainStage.getScene.getRoot.lookup("#main") match {
      case stackPane: StackPane => stackPane
      case s => logger.info(s.getClass.toString); throw new ClassCastException("cos nie tak")
    }
  }

  private def setupDataSources(): Unit = {
    logger.info("Setting up sources...")

    val sources: Array[DataSource] = Array(new WeatherDataSource, new AirDataSource)

    mainSource = sources.map(a => EventStream.joinStream(a.dataSourceStream)).apply(0)
  }

  /**
    * set logging events to console
    */
  private def setupEventHandler(): Unit = {
    val events: Observable[WeatherEvent] = EventStream.events
    events.subscribe(a => logger.info(a))

    events.filter({
      case _: SettingsRequestEvent => true
      case _ => false
    }).subscribe(_ => onSettings())
  }

  private def setupTextRendering() = {
    System.setProperty("prism.text", "t2k")
    System.setProperty("prism.lcdtext", "true")
  }

  private def setupExceptionHandler() = {
    Thread.setDefaultUncaughtExceptionHandler(
      (t: Thread, e: Throwable) => logger.error("Uncaught exception in thread \'" + t.getName + "\'", e)
    )
  }


}

object Main {
  def main(args: Array[String]) {

    Platform.setImplicitExit(true)

    Application.launch(classOf[Gui], args: _*)
  }
}


