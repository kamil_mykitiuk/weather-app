package event

import java.time.LocalDateTime

/**
  * Event of single data input
  */
case class RawRateEvent(timestamp: LocalDateTime, value: Option[Float]) extends WeatherEvent {

  def getTimestamp: LocalDateTime = timestamp

  def getValue: Option[Float] = value

  override def toString: String = "RawRateEvent(timestamp=" + getTimestamp + ", value=" + getValue + ")"
}
