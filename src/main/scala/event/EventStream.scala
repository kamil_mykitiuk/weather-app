package event

import javafx.beans.binding.Binding

import rx.lang.scala.schedulers.IOScheduler
import rx.lang.scala.{JavaConversions, Observable, Subscription}
import rx.schedulers.JavaFxScheduler
import rx.subscribers.JavaFxSubscriber

/**
  * utility functions used to menage main event observable
  */
object EventStream {

  class WrappedObservable[T](inner: Observable[T]) {

    def andOn[U](observable: Observable[U], mapper: U => T): WrappedObservable[T] = {
      new WrappedObservable[T](inner.merge(observable.map[T](mapper)))
    }

    def andOn[U](observable: Observable[U], value: T) = new WrappedObservable[T](inner.merge(fixedMap(observable, value)))

    def toObservable: Observable[T] = inner

    def toBinding: Binding[T] = binding(inner)
  }


  def fixedMap[T](observable: Observable[_], value: T): Observable[T] = observable.map(_ => value)

  /**
    * converting observalbe to binding for JavaFX usage
    */
  def binding[T](observable: Observable[T]): Binding[T] = {
    val javaObservable: rx.Observable[T] = JavaConversions.toJavaObservable[T](observable).asInstanceOf[rx.Observable[T]]
    JavaFxSubscriber.toBinding(javaObservable)
  }

  var composite: util.CompositeObservable[WeatherEvent] = new util.CompositeObservable[WeatherEvent]

  /**
    * returns main EventStream
    *
    * @return
    */
  def events: Observable[_ <: WeatherEvent] = composite.toObservable

  def onEvent[T, U](observable: Observable[T], mapper: T => U): WrappedObservable[U] = {
    new WrappedObservable[U](observable.map(mapper))
  }

  def onEvent[T, U](observable: Observable[T], value: U): WrappedObservable[U] = {
    new WrappedObservable[U](fixedMap(observable, value))
  }

  /**
    * Contribute to main EventStream
    */
  def joinStream(observable: Observable[_ <: WeatherEvent]): Subscription = {
    synchronized(composite.add(observable))
  }


  /**
    * Bunch of transformers for FX and IO usage, multithreading reasons
    */
  def eventInFx: Observable[WeatherEvent] = fxTransformer(events)

  def fxTransformer[T]: Observable[T] => Observable[T] = obs => obs.observeOn(JavaConversions.javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance()))

  def eventsInIO: Observable[WeatherEvent] = ioTransformer(events)

  def ioTransformer[T]: Observable[T] => Observable[T] = obs => obs.observeOn(IOScheduler())

}
