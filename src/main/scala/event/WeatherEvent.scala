package event

import java.time.LocalDateTime

/**
  * Events used to control application
  */
abstract class WeatherEvent {
  override def toString: String = "WeatherEvent()"
}

case class SettingsRequestEvent() extends WeatherEvent {
  override def toString: String = "SetingsRequestEvent()"
}

case class ErrorEvent(cause: Throwable) extends WeatherEvent {
  private var timestamp: LocalDateTime = LocalDateTime.now()

  def getTimestamp: LocalDateTime = timestamp

  def getCause: Throwable = cause

  override def toString: String = "ErrorEvent(timestamp=" + getTimestamp + ", cause" + getCause + ")"

}

case class RefreshRequstEvent() extends WeatherEvent {
  override def toString = "RefreshRequestEvent()"
}

case class NetworkRequestFinishedEvent() extends WeatherEvent {
  override def toString = "NetworkRequestFinishedEvent()"
}

case class NetworkRequestIssuedEvent() extends WeatherEvent {
  override def toString = "NetworkRequestIssuedEvent()"
}

