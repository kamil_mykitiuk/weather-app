package event

import java.time.LocalDateTime

/**
  * Generic class of data carrying event
  */
abstract class RateEvent extends WeatherEvent {
  var timestamp: LocalDateTime = LocalDateTime.now()

  def getTimeStamp: LocalDateTime = this.timestamp

  override def toString: String = "RateEvent(timesatamp=" + this.getTimeStamp + ")"
}


case class WeatherRateEvent(
                             temp: Option[Float],
                             pressure: Option[Float],
                             humidity: Option[Float],
                             windSpeed: Option[Float],
                             windDirection: Option[Float],
                             clouds: Option[Float]) extends RateEvent {


  override def toString: String = {
    "OpenWeatherRateEvent(timestamp=" + timestamp + " temp=" + temp + ")"
  }
}

case class AirRateEvent(PM10: Float, PM25: Float) extends RateEvent {
  override def toString: String = "AirRateEvent(timestamp=" + timestamp + ", value=" + PM10 + ")"
}