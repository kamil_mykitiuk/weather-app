package controller

import java.time.format.{DateTimeFormatter, FormatStyle}
import javafx.beans.value.ObservableValue
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.{Button, Label}

import control.InfoValueControl
import event._
import rx.lang.scala.JavaConversions.{javaSchedulerToScalaScheduler, toScalaObservable}
import rx.lang.scala.Observable
import rx.observables.JavaFxObservable
import rx.schedulers.JavaFxScheduler

import scala.concurrent.duration.{DurationInt, FiniteDuration}

/**
  * Main application controller
  */
class WeatherController {

  val ERROR_MSG_DURRATION: FiniteDuration = 30 seconds

  @FXML
  private var updateTime: Label = _

  @FXML
  private var tempControl: InfoValueControl = _

  @FXML
  private var pressureControl: InfoValueControl = _

  @FXML
  private var windPower: InfoValueControl = _

  @FXML
  private var windDirectionControl: InfoValueControl = _

  @FXML
  private var humidityControl: InfoValueControl = _

  @FXML
  private var cloudControl: InfoValueControl = _

  @FXML
  private var PM10Control: InfoValueControl = _

  @FXML
  private var PM25Control: InfoValueControl = _

  @FXML
  private var errorIcon: Node = _

  @FXML
  private var workingIcon: Node = _

  @FXML
  private var refreshButton: Button = _

  @FXML
  private var settingsButton: Button = _

  @FXML
  private def initialize(): Unit = {
    initializeStatus()
    initializeRefreshHandler()
    initializeSettingsHandler()
    initializeTimeHandler()
  }

  /**
    * make clicking button emmit event
    */
  private def initializeRefreshHandler(): Unit = {
    EventStream.joinStream(toScalaObservable(JavaFxObservable.actionEventsOf(refreshButton)).map(_ => RefreshRequstEvent()))
  }

  /**
    * make clicking button emmit event
    */
  private def initializeSettingsHandler(): Unit = {
    EventStream.joinStream(toScalaObservable(JavaFxObservable.actionEventsOf(settingsButton)).map(_ => SettingsRequestEvent()))
  }

  /**
    * Subscribe updateTimeLabel to corresponding events
    */
  private def initializeTimeHandler(): Unit = {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
    val partial: PartialFunction[WeatherEvent, RateEvent] = {
      case a: RateEvent => a
    }
    EventStream.eventInFx.collect(partial).subscribe(e => updateTime.setText("Ostatnia Aktualizacja: " + e.timestamp.format(formatter)))
  }

  /**
    * initialize fancy icons - working icon and error icon
    */
  private def initializeStatus(): Unit = {
    val events: Observable[WeatherEvent] = EventStream.eventInFx

    val errors: Observable[WeatherEvent] = events.filter({
      case _: ErrorEvent => true
      case _ => false
    })

    //set control of error icon
    val a = EventStream.binding(
      errors.map(_ => true)
        .merge(errors.throttleWithTimeout(ERROR_MSG_DURRATION,
          javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance())).map(_ => false))
    ).asInstanceOf[ObservableValue[_ <: java.lang.Boolean]]
    errorIcon.visibleProperty().bind(a)

    //make working icon visible each time for 2 second between upcoming NetworkRequestEvents
    workingIcon.visibleProperty().bind(EventStream.binding(
      events.filter({
        case _: NetworkRequestIssuedEvent => true
        case _ => false
      }).map(e => 1)
        .merge(events.filter({
          case _: NetworkRequestFinishedEvent => true
          case _ => false
        }).map(e => -1).delay(2 seconds, javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance())))
        .scan(0)((x: Int, y: Int) => x + y).map(a => a > 0)))
  }

  //getters for fxml property values
  def getTempValue: Observable[RawRateEvent] = getDataStream(e => e.temp)

  def getPressureValue: Observable[RawRateEvent] = getDataStream(e => e.pressure)

  def getWindPowerValue: Observable[RawRateEvent] = getDataStream(e => e.windSpeed)

  def getWindDirectionValue: Observable[RawRateEvent] = getDataStream(e => e.windDirection)

  def getHumidityValue: Observable[RawRateEvent] = getDataStream(e => e.humidity)

  def getCloudinessValue: Observable[RawRateEvent] = getDataStream(e => e.clouds)

  def getPM10Value: Observable[RawRateEvent] = getAirStream(e => Some(e.PM10))

  def getPM25Value: Observable[RawRateEvent] = getAirStream(e => Some(e.PM25))

  //convert rating events from EventStrem to RawDataStream of given function values
  def getDataStream(f: WeatherRateEvent => Option[Float]): Observable[RawRateEvent] = {
    val partial: PartialFunction[WeatherEvent, WeatherRateEvent] = {
      case a: WeatherRateEvent => a
    }
    EventStream.eventInFx.collect(partial).map(e => RawRateEvent(e.timestamp, f(e)))
  }

  def getAirStream(f: AirRateEvent => Option[Float]): Observable[RawRateEvent] = {
    val partial: PartialFunction[WeatherEvent, AirRateEvent] = {
      case a: AirRateEvent => a
    }
    EventStream.eventInFx.collect(partial).map(e => RawRateEvent(e.timestamp, f(e)))
  }


}
